<?php

use Illuminate\Support\Facades\Route;

//Route::middleware('ms-gate')->group(function () {
    Route::get('comments/{comment}/childes', 'CommentController@showChildes')->name('comments.childes');
    Route::apiResource('comments', 'CommentController');

    Route::post('comments/{comment}/toggle-like', 'CommentController@toggleLike')->name('comments.toggle-like');
//});
