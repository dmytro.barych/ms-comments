<?php

declare(strict_types=1);

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'text' => Str::random(rand(10, 30)),
            'post_id' => rand(1, 10),
            'user_id' => rand(0, 10),
        ];
    }
}
