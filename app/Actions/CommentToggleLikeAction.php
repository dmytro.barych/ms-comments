<?php

declare(strict_types=1);

namespace App\Actions;

use App\Exceptions\BasicException;
use App\Models\Comment;
use Illuminate\Support\Facades\DB;

class CommentToggleLikeAction
{
    public function handle(Comment $comment, int $userId): ?Comment
    {
        try {
            DB::beginTransaction();

            $like = $comment->likes()->where('user_id', $userId)->first();
            if ($like) {
                $like->delete();
            } else {
                $comment->likes()->create(['user_id' => $userId]);
            }

            DB::commit();

            return $comment;
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }
}
