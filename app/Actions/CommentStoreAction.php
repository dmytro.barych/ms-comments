<?php

declare(strict_types=1);

namespace App\Actions;

use App\Exceptions\BasicException;
use App\Models\Comment;
use Illuminate\Support\Facades\DB;

class CommentStoreAction
{
    public function handle(array $data): ?Comment
    {
        try {
            DB::beginTransaction();

            $comment = new Comment($data);
            $comment->parent()->associate($data['parent_id'] ?? null);
            $comment->save();

            DB::commit();

            return $comment;
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }
}
