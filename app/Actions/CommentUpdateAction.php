<?php

declare(strict_types=1);

namespace App\Actions;

use App\Exceptions\BasicException;
use App\Models\Comment;
use Illuminate\Support\Facades\DB;

class CommentUpdateAction
{
    public function handle(Comment $comment, array $data): ?Comment
    {
        try {
            DB::beginTransaction();

            $comment->fill($data);
            $comment->save();

            DB::commit();

            return $comment;
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }
}
