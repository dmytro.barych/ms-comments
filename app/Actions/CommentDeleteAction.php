<?php

declare(strict_types=1);

namespace App\Actions;

use App\Exceptions\BasicException;
use App\Models\Comment;
use Illuminate\Support\Facades\DB;

class CommentDeleteAction
{
    public function handle(Comment $comment): bool
    {
        try {
            DB::beginTransaction();

            $comment->delete();

            DB::commit();

            return true;
        } catch (BasicException $exception) {
            DB::rollBack();

            return false;
        }
    }
}
