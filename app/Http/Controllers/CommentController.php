<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\CommentDeleteAction;
use App\Actions\CommentStoreAction;
use App\Actions\CommentToggleLikeAction;
use App\Actions\CommentUpdateAction;
use App\Http\Requests\Comment\Index;
use App\Http\Requests\Comment\Store;
use App\Http\Requests\Comment\Update;
use App\Http\Requests\ToggleLikeRequest;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CommentController extends Controller
{
    public function index(Index $request): CommentCollection
    {
        return new CommentCollection(
            Comment::orderBy('created_at', 'desc')
                ->whereNull('parent_id')
                ->whereIn('post_id', $request->post_ids)
                ->withChildes(3)
                ->paginate($request->per_page ?? 3)
        );
    }

    public function store(Store $request, CommentStoreAction $action): CommentResource
    {
        return new CommentResource($action->handle($request->validated()));
    }

    public function show(Comment $comment): CommentResource
    {
        return new CommentResource(
            $comment->load([
                'childes' => function ($q) {
                    $q->orderBy('created_at')->limit(10);
                }
            ])
        );
    }

    public function showChildes(Comment $comment): CommentCollection
    {
        return new CommentCollection(Comment::where('parent_id', $comment->id)->orderBy('created_at')->paginate(10));
    }

    public function update(Update $request, Comment $comment, CommentUpdateAction $action): CommentResource
    {
        return new CommentResource($action->handle($comment, $request->validated()));
    }

    public function destroy(Comment $comment, CommentDeleteAction $action): JsonResponse
    {
        return response()->json(['success' => $action->handle($comment), Response::HTTP_OK]);
    }

    public function toggleLike(
        Comment $comment,
        CommentToggleLikeAction $action,
        ToggleLikeRequest $request
    ): CommentResource {
        return new CommentResource($action->handle($comment, (int)$request->user_id));
    }
}
