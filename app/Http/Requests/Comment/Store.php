<?php

declare(strict_types=1);

namespace App\Http\Requests\Comment;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Store extends FormRequest
{
    public function rules(): array
    {
        return [
            'post_id' => [
                'required',
                'integer'
            ],
            'user_id' => [
                'required',
                'integer'
            ],
            'text' => [
                'required',
                'min:3'
            ],
            'parent_id' => [
                'sometimes',
                Rule::exists('comments', 'id')
                    ->where('post_id', $this->post_id)
                    ->whereNull('parent_id')
            ]
        ];
    }
}
