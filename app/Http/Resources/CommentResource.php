<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    public function toArray($request): array
    {
        $data = [];
        if (!$this->childes->isEmpty()) {
            $data['childes'] = new CommentCollection($this->childes);
        }
        return array_merge([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'post_id' => $this->post_id,
            'text' => $this->text,
            'likes_count' => $this->likes()->count(),
        ], $data);
    }
}
