<?php

declare(strict_types=1);

namespace App\Models;

use App\Traits\CommentRelationTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    use CommentRelationTrait;

    protected $fillable = ['text', 'user_id', 'post_id'];

    public function scopeWithChildes($query, $perPage)
    {
        return $query->with('childes', function ($q) use ($perPage) {
            $q->orderBy('created_at')->limit($perPage ?? 3);
        });
    }
}
