<?php

declare(strict_types=1);

namespace App\Traits;

use App\Models\Comment;
use App\Models\CommentLike;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait CommentRelationTrait
{
    public function childes(): HasMany
    {
        return $this->hasMany(Comment::class, 'parent_id', 'id');
    }

    public function parent(): ?BelongsTo
    {
        return $this->belongsTo(Comment::class, 'parent_id', 'id');
    }

    public function likes(): HasMany
    {
        return $this->hasMany(CommentLike::class);
    }
}
