<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Http\Middleware\MsGateMiddleware;
use App\Models\Comment;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CommentControllerTest extends TestCase
{
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware([MsGateMiddleware::class]);
    }

    /**
     * @test
     */
    public function storeCommentTest()
    {
        $commentArray = [
            'user_id' => 1,
            'post_id' => 1,
            'text' => Str::random(rand(10, 30)),
        ];
        $response = $this->post(route('comments.store', $commentArray));
        $response->assertStatus(Response::HTTP_CREATED);
        $comment = $response->getOriginalContent();
        $this->assertDatabaseHas(Comment::class, [
            'id' => $comment->id,
            'text' => $commentArray['text']
        ]);

        $commentArray = [
            'user_id' => 1,
            'post_id' => 1,
            'text' => Str::random(rand(10, 30)),
            'parent_id' => $comment->id,
        ];
        $response = $this->post(route('comments.store', $commentArray));
        $response->assertStatus(Response::HTTP_CREATED);
        $comment = $response->getOriginalContent();
        $this->assertDatabaseHas(Comment::class, [
            'id' => $comment->id,
            'text' => $commentArray['text'],
            'parent_id' => $commentArray['parent_id']
        ]);
    }

    /**
     * @test
     */
    public function updateCommentTest()
    {
        $commentOld = Comment::factory()->create();
        $commentArray = [$commentOld->id, 'text' => Str::random(rand(10, 30))];
        $response = $this->put(route('comments.update', $commentArray));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas(Comment::class, [
            'id' => $commentOld->id,
            'text' => $commentArray['text'],
            'user_id' => $commentOld->user_id,
            'post_id' => $commentOld->post_id,
        ]);
    }

    /**
     * @test
     */
    public function indexCommentTest()
    {
        Comment::factory()->for(Comment::factory()->create(['post_id' => 0]), 'parent')->create(['post_id' => 0]);
        $response = $this->get(route('comments.index', ['post_ids' => [0]]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(
            [
                'data' => [
                    [
                        'id',
                        'user_id',
                        'post_id',
                        'text',
                        'likes_count',
                        'childes' => [
                            [
                                'id',
                                'user_id',
                                'post_id',
                                'text',
                                'likes_count',
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    /**
     * @test
     */
    public function showCommentTest()
    {
        $comment = Comment::factory()->create(['post_id' => 0]);
        Comment::factory()->for($comment, 'parent')->create(['post_id' => 0]);
        $response = $this->get(route('comments.show', [$comment->id]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'user_id',
                    'post_id',
                    'text',
                    'likes_count',
                    'childes' => [
                        [
                            'id',
                            'user_id',
                            'post_id',
                            'text',
                            'likes_count',
                        ]
                    ]
                ]
            ]
        );
    }

    /**
     * @test
     */
    public function showCommentChildesTest()
    {
        $comment = Comment::factory()->create(['post_id' => 0]);
        Comment::factory()->for($comment, 'parent')->create(['post_id' => 0]);
        $response = $this->get(route('comments.childes', [$comment->id]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(
            [
                'data' => [
                    [
                        'id',
                        'user_id',
                        'post_id',
                        'text',
                        'likes_count'
                    ]
                ]
            ]
        );
        $this->assertEquals($response->getOriginalContent()->first()->parent_id, $comment->id);
    }

    /**
     * @test
     */
    public function destroyCommentTest()
    {
        $comment = Comment::factory()->create(['post_id' => 0]);
        $commentChild = Comment::factory()->for($comment, 'parent')->create(['post_id' => 0]);
        $response = $this->delete(route('comments.destroy', [$comment->id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('comments', ['id' => $comment->id]);
        $this->assertEquals($comment->id, $commentChild->parent_id);
        $commentChild->refresh();
        $this->assertNotEquals($comment->id, $commentChild->parent_id);
    }

    /**
     * @test
     */
    public function toggleCommentLikeTest()
    {
        $comment = Comment::factory()->create(['post_id' => 0]);

        //Like
        $response = $this->post(route('comments.toggle-like', [$comment->id, 'user_id' => $comment->user_id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('comment_likes', ['comment_id' => $comment->id, 'user_id' => $comment->user_id]);

        //Remove like
        $response = $this->post(route('comments.toggle-like', [$comment->id, 'user_id' => $comment->user_id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('comment_likes', ['comment_id' => $comment->id, 'user_id' => $comment->user_id]);
    }
}
